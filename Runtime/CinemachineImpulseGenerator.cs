﻿using UnityEngine;
using Cinemachine;

namespace ActionCode.Cinemachine
{
    /// <summary>
    /// Generates an impulse using the local <see cref="CinemachineImpulseSource"/> component when enabled.
    /// </summary>
    [RequireComponent(typeof(CinemachineImpulseSource))]
    public sealed class CinemachineImpulseGenerator : MonoBehaviour
    {
        [SerializeField] private CinemachineImpulseSource source;

        private void Reset() => source = GetComponent<CinemachineImpulseSource>();
        private void OnEnable() => source.GenerateImpulse();
    }
}